#!/usr/bin/env perl
use Mojo::Base -base;
use Mojo::File;
use Mojo::Pg;
use lib '.';
use WatchListGoodCollection qw($good_collection);

my $good_ones = {};
my $email = 'admin@sympa.tld';

$good_collection->each(sub {
    my ($e, $num) = @_;
    $good_ones->{$e} = 1;
});

my $pg = Mojo::Pg->new('postgresql://postgres@/sympa');
my $h  = $pg->db->select('admin_table',
    [
        'user_admin',
        \'count(list_admin) AS list_nb',
        \'string_agg(list_admin, \' \' ORDER BY list_admin) AS lists'
    ],
    {
        user_admin => {
            -not_like => '%cdg59.teaming.fr'
        }
    },
    {
        group_by => ['user_admin'],
        # Change the trigger level here
        having   => \'count(list_admin) > 3',
        order_by => { -desc => [ 'list_nb', 'user_admin' ] }
    }
)->hashes;
my $suspects = $h->grep(sub {
    return !defined($good_ones->{$_->{user_admin}});
});

my $tmp  = Mojo::File->new('/tmp/bad_list_nb.tmp');
my $file = Mojo::File->new('/tmp/bad_list_nb');

if ($suspects->size == 0) {
    $tmp->spurt('');
    if (! -e '/tmp/bad_list_nb' || $file->slurp ne '') {
        `echo "Résolu" | sed -e "s\@ \+\@ \@g" | mail -a 'Content-Type: text/plain; charset=utf-8' -s '=?UTF-8?Q?[R=C3=A9solu]?= Utilisateurs avec trop de listes' $email`;
        $file->spurt('');
    }
} else {
    $file->touch() unless -e $file;

    my $text  = '';
    my $text2 = '';
    $suspects->each(sub {
        my ($e, $num) = @_;
        $text  .= sprintf("%-50s | %4s | %s\n", $e->{user_admin}, $e->{list_nb}, $e->{lists});
        if ($num % 4 == 0 || $num == $suspects->size) {
            $text2 .= sprintf("'%s',\n", $e->{user_admin});
        } else {
            $text2 .= sprintf("'%s', ", $e->{user_admin});
        }
    });
    $tmp->spurt($text);
    if (! -e '/tmp/bad_list_nb' || $file->slurp ne $text) {
        my $msg = "$text\n$text2";
        `echo "$msg" | mail -a 'Content-Type: text/plain; charset=utf-8' -s 'ALERTE SYMPA !  =?UTF-8?Q?=C3=80?= traiter de toute urgence, Utilisateurs avec trop de listes' $email`;
        $file->spurt($text);
    }
}
